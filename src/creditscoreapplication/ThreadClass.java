/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package creditscoreapplication;

import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdk.nashorn.api.scripting.JSObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 *
 * @author Karoney
 */


public class ThreadClass implements Runnable {
   private Thread t;
   private String threadName;
   
   ThreadClass( String name) {
      threadName = name;
      System.out.println("Creating " +  threadName );
   }
   
   public void run() {
      System.out.println("Strarting " +  threadName );
     
         for(int i = 0; i <1500000; i++) {
            
             
                 JSONObject obj;
       try {
           obj = new JSONObject("{\n" +
                   "   \"loanAccount\":{\n" +
                   "      \"id\":\"Custom_ID\",\n" +
                   "      \"accountHolderKey\":\"40288a13437b71d201437b87aa6300e1\",\n" +
                   "      \"accountHolderType\":\"CLIENT\",\n" +
                   "      \"creationDate\":\"2014-01-13T15:00:11+0200\",\n" +
                   "      \"lastModifiedDate\":\"2014-01-13T15:00:11+0200\",\n" +
                   "      \"accountState\":\"PENDING_APPROVAL\",\n" +
                   "      \"productTypeKey\":\"40288a13437b71d201437b87af7d033f\",\n" +
                   "      \"loanName\":\"Agriculture Loan\",\n" +
                   "      \"loanAmount\":\"950\",\n" +
                   "      \"principalDue\":\"0\",\n" +
                   "      \"principalPaid\":\"0\",\n" +
                   "      \"principalBalance\":\"0\",\n" +
                   "      \"interestDue\":\"0\",\n" +
                   "      \"interestPaid\":\"0\",\n" +
                   "      \"interestBalance\":\"0\",\n" +
                   "      \"feesDue\":\"0\",\n" +
                   "      \"feesPaid\":\"0\",\n" +
                   "      \"feesBalance\":\"0\",\n" +
                   "      \"penaltyDue\":\"0\",\n" +
                   "      \"penaltyPaid\":\"0\",\n" +
                   "      \"penaltyBalance\":\"0\",\n" +
                   "      \"scheduleDueDatesMethod\":\"INTERVAL\",\n" +
                   "      \"repaymentPeriodCount\":1,\n" +
                   "      \"repaymentPeriodUnit\":\"MONTHS\",\n" +
                   "      \"repaymentInstallments\":5,\n" +
                   "      \"gracePeriod\":0,\n" +
                   "      \"gracePeriodType\":\"NONE\",\n" +
                   "      \"interestRate\":\"2.5\",\n" +
                   "      \"interestChargeFrequency\":\"EVERY_FOUR_WEEKS\",\n" +
                   "      \"interestCalculationMethod\":\"DECLINING_BALANCE\",\n" +
                   "      \"repaymentScheduleMethod\":\"FIXED\",\n" +
                   "      \"paymentMethod\":\"HORIZONTAL\",\n" +
                   "      \"interestApplicationMethod\":\"ON_DISBURSEMENT\",\n" +
                   "      \"notes\":\"some_notes\",\n" +
                   "      \"principalRepaymentInterval\":1,\n" +
                   "      \"interestRateSource\":\"FIXED_INTEREST_RATE\",\n" +
                   "      \"interestAdjustment\":\"0\",\n" +
                   "      \"accruedInterest\":\"0\",\n" +
                   "      \"arrearsTolerancePeriod\":10,\n" +
                   "      \"guarantees\":[\n" +
                   "         {\n" +
                   "            \"guarantorKey\":\"8a80806852f38c860152f38de0ad0019\",\n" +
                   "            \"amount\":\"950\",\n" +
                   "            \"guarantorType\":\"GUARANTOR\",\n" +
                   "            \"customFieldValues\":[\n" +
                   "               {\n" +
                   "                  \"CustomFieldID\":\"Employement_status\",\n" +
                   "                  \"value\":\"Self-employed\"\n" +
                   "               }\n" +
                   "            ]\n" +
                   "         }\n" +
                   "      ],\n" +
                   "      \"funds\":[\n" +
                   "         {\n" +
                   "            \"guarantorKey\":\"GUARANTOR_KEY\",\n" +
                   "            \"savingsAccountKey\":\"SAVINGS_ACC_KEY\",\n" +
                   "            \"amount\":\"500\",\n" +
                   "            \"interestCommission\":\"4\"\n" +
                   "         }\n" +
                   "      ]\n" +
                   "   },\n" +
                   "   \"disbursementDetails\":{\n" +
                   "      \"expectedDisbursementDate\":\"2016-01-08\",\n" +
                   "      \"firstRepaymentDate\":\"2016-02-08\",\n" +
                   "      \"transactionDetails\":{\n" +
                   "         \"transactionChannel\":{\n" +
                   "            \"id\":\"RECEIPT\",\n" +
                   "            \"encodedKey\":\"8a8080855281ef57015281ef9bc20436\"\n" +
                   "         },\n" +
                   "         \"receiptNumber\":\"1146\",\n" +
                   "         \"bankNumber\":\"bank1146\"\n" +
                   "      },\n" +
                   "      \"fees\":[\n" +
                   "         {\n" +
                   "            \"fee\":{\n" +
                   "               \"encodedKey\":\"8a8080855283305201528369d0d3001c\"\n" +
                   "            }\n" +
                   "         },\n" +
                   "         {\n" +
                   "            \"fee\":{\n" +
                   "               \"encodedKey\":\"8a8080855283305201528369d0d3001b\"\n" +
                   "            },\n" +
                   "            \"amount\":\"10\"\n" +
                   "         },\n" +
                   "         {\n" +
                   "            \"fee\":{\n" +
                   "               \"encodedKey\":\"8a808085528330520152836c5950002a\"\n" +
                   "            },\n" +
                   "            \"amount\":\"55\"\n" +
                   "         }\n" +
                   "      ]\n" +
                   "   },\n" +
                   "   \"customInformation\":[\n" +
                   "      {\n" +
                   "         \"value\":\"None\",\n" +
                   "         \"customFieldID\":\"Special_Installements_Loan_Accounts\"\n" +
                   "      },\n" +
                   "      {\n" +
                   "         \"value\":\"2\",\n" +
                   "         \"customFieldID\":\"Family_Members_Loan_Accounts\"\n" +
                   "      }\n" +
                   "   ]\n" +
                   "}"); 
       
       
       String pageName = obj.getJSONObject("loanAccount").getString("accountHolderKey");
       int randomNum = ThreadLocalRandom.current().nextInt(1, 10 + 1);
    
           System.out.println(i+". Credit Score for AccountHolderKey: "+pageName+" is "+randomNum);
  
       
       
       
       } catch (JSONException ex) {
           Logger.getLogger(ThreadClass.class.getName()).log(Level.SEVERE, null, ex);
       }

            
             
             
             
         }
    
      System.out.println("Thread " +  threadName + " exiting.");
      
      
      
     

      
      
      
      
      
      
   }
   
   public void start () {
      System.out.println("Starting " +  threadName );
      if (t == null) {
         t = new Thread (this, threadName);
         t.start ();
      }
   }
}